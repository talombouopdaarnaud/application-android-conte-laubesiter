import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DontPage } from './dont.page';

describe('DontPage', () => {
  let component: DontPage;
  let fixture: ComponentFixture<DontPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DontPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DontPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
