import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResulPage } from './resul.page';

describe('ResulPage', () => {
  let component: ResulPage;
  let fixture: ComponentFixture<ResulPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResulPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResulPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
