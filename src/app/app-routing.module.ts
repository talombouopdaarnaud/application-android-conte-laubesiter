import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'integration', loadChildren: './integration/integration.module#IntegrationPageModule' },
  { path: 'tabs/dont', loadChildren: './dont/dont.module#DontPageModule' },
  { path: 'resul', loadChildren: './resul/resul.module#ResulPageModule' }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
