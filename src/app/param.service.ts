import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ParamService {
  public taille?: number;
  public poids?: number;
  public imc?: number;

  constructor() { }
}
