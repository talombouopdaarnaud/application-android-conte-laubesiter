import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntegrationPage } from './integration.page';

describe('IntegrationPage', () => {
  let component: IntegrationPage;
  let fixture: ComponentFixture<IntegrationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntegrationPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntegrationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
